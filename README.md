## DietaApp

Esta aplicación ha sido desarrollada por Beatriz González,
las herramientas que se utilizaron son las siguientes:

1. Java, Android nativo.
2. XML (Diseño de layout).
3. Android Studio.
4. Gestor de base de datos SQLite.
5. JSON.


Descripción de aplicación: consiste en llevarle un seguimiento al usuario, en el que se describe un plan alimenticio:
la cantidad de calorias y carbohidratos que debe consumir en un dia o semana según la información brindad al usuario, App
envía la información nutricional al correo del usuario.