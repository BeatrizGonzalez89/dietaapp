# view AndroidManifest.xml #generated:56
-keep class com.example.dietplan.activity.ActivityAddFood { <init>(...); }

# view AndroidManifest.xml #generated:40
-keep class com.example.dietplan.activity.ActivityAppSettings { <init>(...); }

# view AndroidManifest.xml #generated:64
-keep class com.example.dietplan.activity.ActivityCreateMeal { <init>(...); }

# view AndroidManifest.xml #generated:60
-keep class com.example.dietplan.activity.ActivityDeleteFood { <init>(...); }

# view AndroidManifest.xml #generated:68
-keep class com.example.dietplan.activity.ActivityDeleteMeal { <init>(...); }

# view AndroidManifest.xml #generated:88
-keep class com.example.dietplan.activity.ActivityDialogEditDiet { <init>(...); }

# view AndroidManifest.xml #generated:76
-keep class com.example.dietplan.activity.ActivityDietPlan { <init>(...); }

# view AndroidManifest.xml #generated:72
-keep class com.example.dietplan.activity.ActivityEditMeal { <init>(...); }

# view AndroidManifest.xml #generated:52
-keep class com.example.dietplan.activity.ActivityFood { <init>(...); }

# view AndroidManifest.xml #generated:36
-keep class com.example.dietplan.activity.ActivityHome { <init>(...); }

# view AndroidManifest.xml #generated:48
-keep class com.example.dietplan.activity.ActivityManageMeal { <init>(...); }

# view AndroidManifest.xml #generated:44
-keep class com.example.dietplan.activity.ActivityProfile { <init>(...); }

# view AndroidManifest.xml #generated:80
-keep class com.example.dietplan.activity.ActivitySchedule { <init>(...); }

# view AndroidManifest.xml #generated:84
-keep class com.example.dietplan.activity.ActivitySelect { <init>(...); }

# view AndroidManifest.xml #generated:27
-keep class com.example.dietplan.activity.ActivitySplash { <init>(...); }

# view AndroidManifest.xml #generated:121
-keep class com.example.dietplan.activity.AlarmDetailsActivity { <init>(...); }

# view AndroidManifest.xml #generated:125
-keep class com.example.dietplan.activity.AlarmListActivity { <init>(...); }

# view AndroidManifest.xml #generated:155
-keep class com.example.dietplan.activity.AlarmManagerHelper { <init>(...); }

# view AndroidManifest.xml #generated:129
-keep class com.example.dietplan.activity.AlarmScreen { <init>(...); }

# view AndroidManifest.xml #generated:151
-keep class com.example.dietplan.activity.AlarmService { <init>(...); }

# view res/layout/activity_details.xml #generated:102
# view res/layout/activity_details.xml #generated:113
# view res/layout/activity_details.xml #generated:124
# view res/layout/activity_details.xml #generated:135
# view res/layout/activity_details.xml #generated:52
# view res/layout/activity_details.xml #generated:69
# view res/layout/activity_details.xml #generated:80
# view res/layout/activity_details.xml #generated:91
-keep class com.example.dietplan.activity.CustomSwitch { <init>(...); }

# view AndroidManifest.xml #generated:113
-keep class com.example.dietplan.activity.MedicineReminder { <init>(...); }

# view AndroidManifest.xml #generated:117
-keep class com.example.dietplan.activity.MedicineReminderView { <init>(...); }

# view AndroidManifest.xml #generated:109
# view AndroidManifest.xml #generated:161
-keep class com.example.dietplan.activity.SendMail { <init>(...); }

# view AndroidManifest.xml #generated:105
-keep class com.example.dietplan.activity.ViewSchedule { <init>(...); }

# view AndroidManifest.xml #generated:94
-keep class com.example.dietplan.alarm.AlarmReceiver { <init>(...); }

# view res/xml/app_settings.xml #generated:63
# view res/xml/app_settings.xml #generated:68
# view res/xml/app_settings.xml #generated:73
# view res/xml/app_settings.xml #generated:78
-keep class com.example.dietplan.utils.TimePreference { <init>(...); }

# onClick land\res/layout-land/activity_managemeal.xml #generated:59
# onClick res/layout/activity_managemeal.xml #generated:58
-keepclassmembers class * { *** addFood(...); }

# onClick res/layout/dialogbox_edit_diet.xml #generated:37
-keepclassmembers class * { *** cancelDiet(...); }

# onClick res/layout/activity_select.xml #generated:407
-keepclassmembers class * { *** clearBtn(...); }

# onClick res/layout/activity_createmeal.xml #generated:52
-keepclassmembers class * { *** clearMeal(...); }

# onClick land\res/layout-land/activity_managemeal.xml #generated:120
# onClick res/layout/activity_managemeal.xml #generated:30
-keepclassmembers class * { *** createMeal(...); }

# onClick res/layout/activity_select.xml #generated:373
-keepclassmembers class * { *** delBtn(...); }

# onClick land\res/layout-land/activity_managemeal.xml #generated:85
# onClick res/layout/activity_managemeal.xml #generated:84
-keepclassmembers class * { *** deleteFood(...); }

# onClick land\res/layout-land/activity_managemeal.xml #generated:146
# onClick res/layout/activity_deletemeal.xml #generated:19
# onClick res/layout/activity_managemeal.xml #generated:110
-keepclassmembers class * { *** deleteMeal(...); }

# onClick land\res/layout-land/activity_home.xml #generated:80
-keepclassmembers class * { *** dietPlanBtn(...); }

# onClick res/layout/activity_deletemeal.xml #generated:29
# onClick res/layout/dialogbox_edit_diet.xml #generated:27
-keepclassmembers class * { *** eMeal(...); }

# onClick res/layout/activity_editmeal.xml #generated:19
# onClick res/layout/activity_managemeal.xml #generated:136
-keepclassmembers class * { *** editMeal(...); }

# onClick land\res/layout-land/activity_managemeal.xml #generated:24
-keepclassmembers class * { *** foodDetails(...); }

# onClick land\res/layout-land/activity_home.xml #generated:106
# onClick res/layout/activity_editmeal.xml #generated:29
-keepclassmembers class * { *** mealBtn(...); }

# onClick res/layout/activity_managemeal.xml #generated:192
-keepclassmembers class * { *** med(...); }

# onClick res/layout/activity_select.xml #generated:385
-keepclassmembers class * { *** newmeal(...); }

# onClick res/layout/activity_select.xml #generated:229
-keepclassmembers class * { *** nextBtn(...); }

# onClick res/layout/activity_select.xml #generated:311
-keepclassmembers class * { *** previousBtn(...); }

# onClick land\res/layout-land/activity_home.xml #generated:50
-keepclassmembers class * { *** profileBtn(...); }

# onClick res/layout/activity_editmeal.xml #generated:38
-keepclassmembers class * { *** refBtn(...); }

# onClick res/layout/activity_deletemeal.xml #generated:38
-keepclassmembers class * { *** refreshMeal(...); }

# onClick res/layout/activity_createmeal.xml #generated:31
-keepclassmembers class * { *** saveMeal(...); }

# onClick res/layout/activity_select.xml #generated:322
-keepclassmembers class * { *** savedietBtn(...); }

# onClick land\res/layout-land/activity_dietplan.xml #generated:94
# onClick res/layout/activity_createmeal.xml #generated:42
# onClick res/layout/activity_select.xml #generated:396
-keepclassmembers class * { *** scheduleBtn(...); }

# onClick land\res/layout-land/activity_dietplan.xml #generated:123
-keepclassmembers class * { *** selectdietBtn(...); }

# onClick res/layout/activity_managemeal.xml #generated:164
-keepclassmembers class * { *** shareFile(...); }

